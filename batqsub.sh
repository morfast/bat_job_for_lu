#!/bin/bash


if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <parent directory> [MIN_JOB_NUM  MAX_JOB_NUM]"
    exit 1
fi

if [[ $# -eq 3 ]]; then
    if [[ $2 -ge $3 ]]; then
	echo "MIN_JOB_NUM should be smaller than MAX_JOB_NUM"
	exit 1
    fi
    MIN_JOB_NUM=$2
    MAX_JOB_NUM=$3
else
    source config
fi


if [[ ! -f $1/a.out ]]; then
    echo "$1/a.out not found"
    exit 1
fi

PARENT_DIR_NAME=$(echo ${1} | sed "s/\/*$//g")
JOB_NUM=${MIN_JOB_NUM}
while :
do
    NEWDIR=${PARENT_DIR_NAME}_${JOB_NUM}
    if [ ! -d ${NEWDIR} ]; then
        mkdir -v $NEWDIR
    fi
    cp $1/a.out $NEWDIR
    cp $1/make.sh $NEWDIR
    cd $NEWDIR
    qsub make.sh
    cd -

    JOB_NUM=$(( JOB_NUM + 1))
    if [[ ${JOB_NUM} -gt ${MAX_JOB_NUM} ]]; then
        break
    fi

done
