#!/usr/bin/python

import sys

def Open(filename, mode = 'r'):
    try:
        file = open(filename, mode)
    except IOError,e:
        print "file open error: %s" % (filename)
        sys.exit(1)
    return file

def getData(filename):
    steps = []
    datas = []
    lines = Open(filename).readlines()
    for line in lines:
        line_num = [float(num) for num in line.strip().split()]
        if line_num == []:
            continue
        steps.append(line_num[0])
        datas.append(line_num[1:])

    #print steps
    #print datas

    return steps, datas

def sum_datas(datas1, datas2):
    assert(len(datas1) == len(datas2))
    res = []

    for i,line1 in enumerate(datas1):
        line2 = datas2[i]
        assert(len(line1) == len(line2))
        subres = []
        for j,num1 in enumerate(line1):
            num2 = line2[j]
            subres.append(num1+num2)
        res.append(subres)

    return res

def check_steps(steps1, steps2):
    length = len(steps1)
    i = 0
    while i < length:
        assert(steps1[i] == steps2[i])
        i += 1 

def output(steps, datas, ofilename):
    ofile = Open(ofilename, 'w')
    for i,s in enumerate(steps):
        line = datas[i]
        line.insert(0, s)
        line = ["%.8lf" % i for i in line]
        line = '    '.join(line)
        ofile.write(line)
        ofile.write('\n')

def read_data_filename():
    try:
	namefile = open("datafilenames")
    except IOError,e:
	namefile = Open("/home/luhan/bin/bat_job_for_lu/datafilenames")
    names = namefile.readlines()
    names = [n.strip() for n in names]
    return names
    

def get_avg(datafilename):
    dir_names = sys.argv[1:]
    steps0,datas_sum = getData("/".join([dir_names[0], datafilename]))
    for dir_name in dir_names[1:]:
        filename = "/".join([dir_name, datafilename])
        steps, datas = getData(filename)
        check_steps(steps, steps0)
        datas_sum = sum_datas(datas_sum, datas)

    n_file = len(sys.argv) - 1
    for i,line in enumerate(datas_sum):
        for j,num1 in enumerate(line):
            line[j] = num1/n_file

    #print datas_sum
    output(steps0, datas_sum, datafilename+'.result')

if len(sys.argv) < 3:
    print "Usage: %s <result directories>" % (sys.argv[0])
    exit(1)
    

data_filenames = read_data_filename()
for filename in data_filenames:
    sys.stderr.write("generating %s.result..." % filename)
    get_avg(filename)
    sys.stderr.write("OK\n")
